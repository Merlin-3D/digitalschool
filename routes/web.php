<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/','index.index');

//Routes Année scolaire
Route::view('ajoutAnnee','annees.ajoutAnnee');
Route::view('listeAnnee','annees.listeAnnee');
//End

//Routes Classe
Route::view('ajoutClasse','classes.ajouterClasse');
Route::view('gestionClasse','classes.gestionClasse');
Route::view('listeClasse','classes.listeClasse');
Route::view('fixePrix','classes.fixePrix');

//End

//Routes Inscrition
Route::view('inscription','inscriptions.inscription');
Route::view('eleveInscrit','inscriptions.eleveInscrit');
