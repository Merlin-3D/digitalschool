@extends('layouts.layaoutPrincipale')
@section('title')
  Elèves Inscrits
@endsection
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Eleves Inscrits</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Acceuil</a></li>
              <li class="breadcrumb-item active">Inscrit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">


          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Listes des élèves en régle</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Matricule</th>
                    <th>Photo</th>
                    <th>Nom</th>
                    <th>Prenom</th>
                    <th>Sexe</th>
                    <th>Classe</th>
                    <th>Statut</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>2019/2020</td>
                  <td><img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2"></td>
                  <td>Deffo</td>
                  <td>Duplex</td>
                  <td>M</td>
                  <td>Terminale</td>
                  <td><span class="badge badge-success">Inscrit</span></td>
                  <td><a class="btn btn-primary btn-sm" href="#">
                      <i class="fas fa-folder">
                      </i>
                      View
                  </a>
                  <a class="btn btn-info btn-sm" href="#">
                      <i class="fas fa-pencil-alt">
                      </i>
                      Edit
                  </a>
                  <a class="btn btn-danger btn-sm" href="#">
                      <i class="fas fa-trash">
                      </i>
                      Delete
                  </a></td>
                </tr>
                <tr>
                  <td>2019/2020</td>
                  <td><img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2"></td>
                  <td>Deffo</td>
                  <td>Duplex</td>
                  <td>M</td>
                  <td>Terminale</td>
                  <td><span class="badge badge-success">Inscrit</span></td>

                  <td><a class="btn btn-primary btn-sm" href="#">
                      <i class="fas fa-folder">
                      </i>
                      View
                  </a>
                  <a class="btn btn-info btn-sm" href="#">
                      <i class="fas fa-pencil-alt">
                      </i>
                      Edit
                  </a>
                  <a class="btn btn-danger btn-sm" href="#">
                      <i class="fas fa-trash">
                      </i>
                      Delete
                  </a></td>
                </tr>
                <tr>
                  <td>2019/2020</td>
                  <td><img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2"></td>
                  <td>Deffo</td>
                  <td>Duplex</td>
                  <td>M</td>
                  <td>Terminale</td>
                  <td><span class="badge badge-success">Inscrit</span></td>

                  <td><a class="btn btn-primary btn-sm" href="#">
                      <i class="fas fa-folder">
                      </i>
                      View
                  </a>
                  <a class="btn btn-info btn-sm" href="#">
                      <i class="fas fa-pencil-alt">
                      </i>
                      Edit
                  </a>
                  <a class="btn btn-danger btn-sm" href="#">
                      <i class="fas fa-trash">
                      </i>
                      Delete
                  </a></td>
                </tr>

                <tfoot>
                <tr>
                  <th>Matricule</th>
                  <th>Photo</th>
                  <th>Nom</th>
                  <th>Prenom</th>
                  <th>Sexe</th>
                  <th>Classe</th>
                  <th>Statut</th>
                  <th>Actions</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>


        </div>
        <!-- /.card-body -->
        <div class="card-footer">

        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection
@section('scripts')
  <script src="../dist/js/pages/dashboard2.js"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
      });
    });
  </script>
@endsection
