@extends('layouts.layaoutPrincipale')
@section('title')
  Nouvelle Inscrition
@endsection
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Inscription</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Acceuil</a></li>
              <li class="breadcrumb-item active">Inscription</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"></h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">

          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Nouvelle Inscription</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-6">
                    <!-- text input -->
                    <div class="form-group">
                      <label>Matricule</label>
                      <input type="text" class="form-control" id="matricule" name="matricule" placeholder="E1011DF">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Année</label>
                      <select class="form-control " >
                        <option selected="selected">-------</option>
                        <option value="">2019 /2020</option>
                        <option value="">2018/2019</option>

                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Nom</label>
                      <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom ...">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <!-- text input -->
                    <div class="form-group">
                      <label>Prénom</label>
                      <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Prénom ...">
                    </div>
                  </div>
                </div>
                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Sexe</label>
                      <select class="form-control " id="classe" name="classe" >
                        <option selected="selected">-------</option>
                        <option value="">M</option>
                        <option value="">F</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <!-- text input -->
                    <div class="form-group">
                      <label>Nationalité</label>
                      <input type="text" class="form-control" id="nationalite" name="nationalite" placeholder="Nationalité ...">
                    </div>
                  </div>

                </div>
                <div class="row">

                  <div class="col-sm-6">
                    <!-- text input -->
                    <div class="form-group">
                      <label>Classe</label>
                      <select class="form-control " id="classe" name="classe" >
                        <option selected="selected">-------</option>
                        <option value="">Premiere</option>
                        <option value="">Terminale</option>

                      </select>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <!-- text input -->
                    <div class="form-group">
                      <label>Status</label>
                      <select class="form-control " id="status" name="status" >
                        <option selected="selected">-------</option>
                        <option value="">Compléte</option>
                        <option value="">Non Compléte</option>

                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">

                  <div class="col-sm-6">
                    <!-- text input -->
                    <div class="form-group">
                      <label>Premiere Tranche</label>
                      <select class="form-control " id="p_tranche" name="p_tranche" >
                        <option selected="selected">-------</option>
                        <option value="">5000</option>
                        <option value="">5100</option>

                      </select>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <!-- text input -->
                    <div class="form-group">
                      <label>Deuxiéme Tranche</label>
                      <select class="form-control " id="d_tranche" name="d_tranche" >
                        <option selected="selected">-------</option>
                        <option value="">5000</option>
                        <option value="">5100</option>

                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-info">Inscrire</button>
              </div>
            </form>
          </div>

        </div>
        <!-- /.card-body -->
        <div class="card-footer">

        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection
