@extends('layouts.layaoutPrincipale')
@section('title')
  Nouvelle Classe
@endsection

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Ajouter Une Classe</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/">Acceuil</a></li>
                <li class="breadcrumb-item active">Classes</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">

        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title"></h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
          </div>
          <div class="card-body ">


            <div class="col-md-4 mx-auto">
              <div class="card card-info ">
                <div class="card-header ">
                  <h3 class="card-title ">Classe</h3>

                  <div class="card-tools ">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                      <i class="fas fa-minus"></i></button>
                  </div>
                </div>
                <div class="card-body">

                  <div class="form-group">
                    <label for="inputSpentBudget">Nouvelle Classe</label>
                    <input type="text" id="classe" name="classe" class="form-control">
                  </div>

                  <div class="form-group">
                    <label for="inputSpentBudget">Capacité</label>
                    <input type="number" id="capacite" name="capacite" class="form-control">
                  </div>

                </div>

                <!-- /.card-body -->
              </div>
              <button type="button" name="button" class="btn btn-info float-right"><i class="fas fa-plus-square"></i> Ajouter une classe</button>

              <!-- /.card -->
            </div>





          </div>
          <!-- /.card-body -->
          <div class="card-footer">

          </div>
          <!-- /.card-footer-->
        </div>
        <!-- /.card -->

      </section>
      <!-- /.content -->
    </div>
@endsection
