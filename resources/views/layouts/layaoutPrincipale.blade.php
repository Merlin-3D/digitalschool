<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>@yield('title')</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="../plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="../../plugins/daterangepicker/daterangepicker.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="../plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="../plugins/datatables-bs4/css/dataTables.bootstrap4.css">

  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <link rel="stylesheet" href="../plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <link rel="stylesheet" href="../plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">

</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index2.html" class="nav-link"><i class="fas fa-cog"></i> Paramétres</a>
      </li>

    </ul>

    <!-- SEARCH FORM -->
    <ul class="navbar-nav ml-auto">
          <button type="button" class="btn btn-block btn-secondary disabled">2010/2020</button>
    </ul>

    <!-- Right navbar links -->

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Digital School</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/AdminLTELogo.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Etablissement1</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item ">
            <a href="/" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>

          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-university"></i>
              <p>
                Classes
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="ajoutClasse" class="nav-link">
                  <i class="fas fa-plus-square nav-icon"></i>
                  <p>Ajouter Classe</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="listeClasse" class="nav-link">
                  <i class="fas fa-list-ul nav-icon"></i>
                  <p>Listes Des Classes</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="gestionClasse" class="nav-link">
                  <i class="fas fa-graduation-cap nav-icon"></i>
                  <p>Gestion des classes</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Inscription
                <i class="fas fa-angle-left right"></i>

              </p>
            </a>
            <ul class="nav nav-treeview">

              <li class="nav-item">
                <a href="inscription" class="nav-link">
                  <i class="fas fa-plus-square nav-icon"></i>
                  <p>Nouvelle Inscription</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="eleveInscrit" class="nav-link">
                  <i class="fas fa-list-ul nav-icon"></i>
                  <p>Listes Des Inscriptions</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item ">
            <a href="./index2.html" class="nav-link">
              <i class="nav-icon far fa-calendar-alt"></i>
              <p>
                Emploi du Temps
              </p>
            </a>

          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-sticky-note"></i>
              <p>
                Modules
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/charts/chartjs.html" class="nav-link">
                  <i class="fas fa-plus-square nav-icon"></i>
                  <p>Ajouter Modules</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/charts/flot.html" class="nav-link">
                  <i class="fas fa-greater-than nav-icon"></i>
                  <p>Affecter Un Module</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/charts/inline.html" class="nav-link">
                  <i class="fas fa-list-ul nav-icon"></i>
                  <p>Listes Modules</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user-graduate"></i>
              <p>
                Professeurs
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/UI/general.html" class="nav-link">
                  <i class="fas fa-plus-square nav-icon"></i>
                  <p>Ajouter Professeurs</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/UI/icons.html" class="nav-link">
                  <i class="fas fa-list-ul nav-icon"></i>
                  <p>Listes Professeurs</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/UI/buttons.html" class="nav-link">
                  <i class="fas fa-greater-than nav-icon"></i>
                  <p>Affecter Un Professeurs</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Abscences
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/forms/general.html" class="nav-link">
                  <i class="fas fa-plus-square nav-icon"></i>
                  <p>Abscences Elèves</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/forms/advanced.html" class="nav-link">
                  <i class="fas fa-plus-square nav-icon"></i>
                  <p>Abscences Professeurs</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item ">
            <a href="./index2.html" class="nav-link">
              <i class="nav-icon fas fa-bus-alt"></i>
              <p>
                Transport
              </p>
            </a>
          </li>
          <li class="nav-item ">
            <a href="./index2.html" class="nav-link">
              <i class="nav-icon fas fa-utensils"></i>
              <p>
                Cantine
              </p>
            </a>
          </li>
          <li class="nav-item ">
            <a href="fixePrix" class="nav-link">
              <i class="nav-icon far fa-money-bill-alt"></i>
              <p>
                Fixation Des Prix
              </p>
            </a>
          </li>
          <li class="nav-item ">
            <a href="./index2.html" class="nav-link">
              <i class="nav-icon fas fa-coins"></i>
              <p>
                Compte
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-clock"></i>
              <p>
                Années
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="ajoutAnnee" class="nav-link">
                  <i class="fas fa-plus-square nav-icon"></i>
                  <p>Ajouter Années</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="listeAnnee" class="nav-link">
                  <i class="fas fa-list-ul nav-icon"></i>
                  <p>Listes de Années</p>
                </a>
              </li>
            </ul>
          </li>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  @yield('content')

<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="../plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.js"></script>

<script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="../dist/js/demo.js"></script>

<script src="../dist/js/pages/dashboard2.js"></script>

<script src="../plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->




@yield('scripts')
</body>
</html>
