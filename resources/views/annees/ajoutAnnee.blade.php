@extends('layouts.layaoutPrincipale')
@section('title')
  Nouvelle Année
@endsection

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Ajouter Une Années</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Acceuil</a></li>
              <li class="breadcrumb-item active">Années</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"></h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body ">


          <div class="col-md-4 mx-auto">
            <div class="card card-info ">
              <div class="card-header">
                <h3 class="card-title">Année</h3>

                <div class="card-tools ">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">

                <div class="form-group">
                  <label for="inputSpentBudget">Nouvelle Année</label>
                  <input type="text" id="annee" name="annee" class="form-control">
                </div>

              </div>

              <!-- /.card-body -->
            </div>
            <div class="form-group">
              <button type="button" name="button" class="btn btn-info col-md-offset-3"><i class="fas fa-plus-square"></i> Ajouter une année</button>
            </div>

            <!-- /.card -->
          </div>





        </div>
        <!-- /.card-body -->
        <div class="card-footer">

        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection

@section('scripts')

@endsection
